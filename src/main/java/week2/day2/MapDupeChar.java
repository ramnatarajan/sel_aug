package week2.day2;

import java.util.Map;
import java.util.TreeMap;

public class MapDupeChar {

	public static void main(String[] args) {
	
		String s = "Lakshmanan Natarajan";
		char[] c = s.toCharArray();
		Map<Character, Integer> m = new TreeMap<Character, Integer>();
		for (char eachChar : c) {
			if (m.containsKey(eachChar)) {
				System.out.println(eachChar);
			}
			else m.put(eachChar, 1);
		}
		
	}

}
