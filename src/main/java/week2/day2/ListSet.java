package week2.day2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class ListSet {
	
	
	public static void main(String[] args) {
		Set<String> s = new TreeSet<String>();
		s.add("Nokia");
		s.add("iPhone");
		s.add("Samsung");
		s.add("LG");
		s.add("Samsung");
		
		for (String eachName : s) {
			System.out.println(eachName);
		}
		System.out.println(" ");
		List<String> l = new ArrayList<String>();
		l.addAll(s);
		String s2 = l.get(1);
		System.out.println(s2);
		}
}



