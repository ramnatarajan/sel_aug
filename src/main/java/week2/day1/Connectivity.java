package week2.day1;

public interface Connectivity {

	public void wifiEnable();
	public void insertSIM();
}
