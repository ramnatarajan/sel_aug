package week2.day1;

public class Samsung extends MobilePhone{

	public void SamsBrowser() { 
		System.out.println("Opening Samsung browser");
		
	}
	public void Gallery() {
		System.out.println("Opening Gallery");
		
	}
	public void GearApp() {
		System.out.println("Opening Gear app");
	}
	
	//Overriding - Same method same signature in different class.
	public void dialCaller() {
		System.out.println("Enter the number through S-Pen");
	}
	//Overloading - Same method name with different signature or arguments
	public void SamsBrowser(int a) {
		System.out.println("Lauch browser in private mode");
	}
	}




