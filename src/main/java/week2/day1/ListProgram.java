package week2.day1;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListProgram {

	public static void main(String[] args) {
		List <String> mobile = new ArrayList<String>();
		//List of mobile phones
		mobile.add("Nokia");
		mobile.add("Apple");
		mobile.add("Samsung");
		mobile.add("LG");

		//Getting the count of mobile phones
		int count = mobile.size();
		System.out.println("Total count of mobile phones used is "+count);
		
		//Printing the last but one mobile
		System.out.println("The last phone is the list is " + mobile.get(count-1));
		
		//Sorting the list in ASCII order
		Collections.sort(mobile);
		System.out.println(mobile);
		
		//Insert an item in the middle
		mobile.add(2, "One Plus");
		System.out.println(mobile);
	}

}
