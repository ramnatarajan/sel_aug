package week2.day1;

public class MobilePhone extends TeleCom implements Connectivity{

	public void dialCaller() {
		System.out.println("Dial through keypad");
	}
	
	public void sendSMS() {
		System.out.println("Send SMS");
	}
	public void addContact() {
		System.out.println("Add a contact");
	}

	@Override
	public void wifiEnable() {
		System.out.println("Wi-Fi is enabled");
		
	}

	@Override
	public void insertSIM() {
		System.out.println("Please insert the sim");
		
	}
}
