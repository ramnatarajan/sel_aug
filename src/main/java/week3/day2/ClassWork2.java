package week3.day2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class ClassWork2 {
	

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leafground.com/pages/table.html");
		String s = driver.findElementByXPath("//table/tbody/tr[3]/td[2]").getText();
		System.out.println(s);
		driver.findElementByXPath("//table/tbody/tr[3]/td[2]/following-sibling::td").click();
		
		//Thread.sleep(10);
	}
	
}
