package week3.day2;


import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.ClickAction;

public class ClassWork {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leafground.com/pages/table.html");
		List<WebElement> chkbox = driver.findElementsByXPath("//input[@type='checkbox']");
		System.out.println(chkbox.size());
		int a = chkbox.size();
		chkbox.get(a-1).click();
		//driver.findElementByXPath("(//input[@type='checkbox'])[3]").click();
		
		Thread.sleep(1000);
		driver.close();
	}

}
