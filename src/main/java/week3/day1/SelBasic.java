package week3.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class SelBasic {

	public static void main(String[] args) {
		//Adding the webdriver to Selenium
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		//Launching the Chrome browser
		ChromeDriver driver = new ChromeDriver();
		//Maximizing the window
		driver.manage().window().maximize();
		//Entering the URL
		try {
			driver.get("http://leaftaps.com/opentaps/control/main");
			//Waiting implicitly to execute the code
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			//Using try catch to capture the exception
			try {
				driver.findElementById("usernamead").sendKeys("DemoSalesManager");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
			driver.findElementById("password").sendKeys("crmsfa");
			driver.findElementByClassName("decorativeSubmit").click();
			driver.findElementByLinkText("CRM/SFA").click();
			driver.findElementByLinkText("Create Lead").click();
			driver.findElementById("createLeadForm_companyName").sendKeys("HCL");
			driver.findElementById("createLeadForm_firstName").sendKeys("Ram");
			driver.findElementById("createLeadForm_lastName").sendKeys("N");
			//Creating a method for selecting a value from dropdown
			WebElement dropdown = driver.findElementById("createLeadForm_dataSourceId");
			Select value = new Select(dropdown);
			value.selectByVisibleText("Employee");
			WebElement dropdown2=driver.findElementById("createLeadForm_marketingCampaignId");
			Select value2 = new Select(dropdown2);
			//Getting the list of values from the dropdown
			List<WebElement> lst = value2.getOptions();
			//Getting the size of the values in the dropdown
			int a = lst.size();
			value2.selectByIndex(3);
			driver.findElementByName("submitButton").click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			//System.out.println("No such element exepction");
			throw new RuntimeException();
		}
		finally {
			driver.close();
		}
	}
			
}

