package week3.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class HomeWork {

	public static void main(String[] args) throws InterruptedException  {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
/*		driver.findElementById("userRegistrationForm:userName").sendKeys("testing");
		driver.findElementById("userRegistrationForm:password").sendKeys("ram_lax1984");*/
		
		driver.findElementById("userRegistrationForm:firstName").sendKeys("ram");
		driver.findElementById("userRegistrationForm:middleName").sendKeys("S");
		driver.findElementById("userRegistrationForm:lastName").sendKeys("Natrajan");
		driver.findElementById("userRegistrationForm:gender:0").click();
		driver.findElementById("userRegistrationForm:maritalStatus:0").click();
		WebElement day = driver.findElementById("userRegistrationForm:dobDay");
		Select value = new Select(day);
		value.selectByVisibleText("30");
		WebElement month = driver.findElementById("userRegistrationForm:dobMonth");
		Select value2 = new Select(month);
		value2.selectByVisibleText("OCT");
		WebElement year = driver.findElementById("userRegistrationForm:dateOfBirth");
		Select value3 = new Select(year);
		value3.selectByVisibleText("1984");
		WebElement work = driver.findElementById("userRegistrationForm:occupation");
		Select value4 = new Select(work);
		List<WebElement> lst = value4.getOptions();
		int a = lst.size();
		value4.selectByIndex(3);
		driver.findElementById("userRegistrationForm:uidno").sendKeys("783553927846");
		driver.findElementById("userRegistrationForm:idno").sendKeys("AJDLI0975M");
		WebElement country = driver.findElementById("userRegistrationForm:countries");
		Select value5 = new Select(country);
		value5.selectByVisibleText("India");
		driver.findElementById("userRegistrationForm:email").sendKeys("ramnatrajan@gmail.com");
		//driver.findElementById("userRegistrationForm:isdCode").sendKeys("91");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("9884204020");
		WebElement nationality = driver.findElementById("userRegistrationForm:nationalityId");
		Select value6 = new Select(nationality);
		value6.selectByVisibleText("India");
		driver.findElementById("userRegistrationForm:address").sendKeys("ejfhuiwge");
		driver.findElementById("userRegistrationForm:street").sendKeys("efjhwui, rur");
		driver.findElementById("userRegistrationForm:area").sendKeys("bfuiwehfuiw");
		//WebElement pin = 
		driver.findElementById("userRegistrationForm:pincode").sendKeys("600077", Keys.TAB);
		Thread.sleep(1000);
		WebElement city = driver.findElementById("userRegistrationForm:cityName");
		Select value7 = new Select(city);
/*		List<WebElement> lst1 = value7.getOptions();
		int b = lst1.size();*/
		value7.selectByVisibleText("Tiruvallur");
		Thread.sleep(1000);
		WebElement post = driver.findElementById("userRegistrationForm:postofficeName");
		Select value8 = new Select(post);
		value8.selectByVisibleText("Thiruverkadu S.O");
		driver.findElementById("userRegistrationForm:landline").sendKeys("28746054");
		
		
		//Thread.sleep(1000);
		driver.close();
	}

}
