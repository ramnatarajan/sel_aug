package javaTraining;

import java.util.Scanner;

public class MultTable {

	public static void main(String[] args) {
		int a, b;
		Scanner n = new Scanner(System.in);
		a = n.nextInt();
		System.out.println("Multiplication table of "+a+" is ");
		for (b=1; b <= 12; b++)
		{
			System.out.println(b+"*"+a+"="+(a*b));
		}
	}

}
