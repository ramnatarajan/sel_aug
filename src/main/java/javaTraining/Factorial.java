package javaTraining;

import java.util.Scanner;

public class Factorial {

	public static void main(String[] args) {
		int a, b, c=1;
		System.out.println("Enter the number to calculate the factorial");
		Scanner v = new Scanner(System.in);
		a = v.nextInt();
		for (b=1; b<=a; b++)
		{
			c = c*b;
		}
		System.out.println(c);
	}
}
