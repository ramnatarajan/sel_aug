package week8.day2;

import java.util.Scanner;

public class ClassRoomSession1 {

	public static void main(String[] args) {
		String str;
		int count = 0;
		Scanner scan = new Scanner(System.in);
		str = scan.nextLine();
		char[] chr = str.toCharArray();
		char input = 'a';
		int size = chr.length;
		for(int i = 0; i<size; i++) {
			if (input==chr[i]) {
				count++;
			}
		}
		System.out.println("Number of "+input+" is "+count);
		
	}

}
