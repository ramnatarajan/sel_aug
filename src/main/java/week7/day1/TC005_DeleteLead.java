package week7.day1;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC005_DeleteLead extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName = "TC005_DeleteLead";
		testCaseDesc = "Create a Lead in Leaftaps";
		category = "Regression";
		author = "Ram Natarajan";
		testData = "DeleteLead";
	}
	
	@Test (dataProvider = "test data")
	public void deleteLead(String username, String password, String countryCode, String phoneNumber, String searchResult) throws InterruptedException {
		new LoginPage().typeUserName(username).typePassword(password).eleLogin().clickcrmsfa().
		clickLeadsButton().clickFindLeads().clickPhoneTab().enterCountryCode(countryCode).
		enterPhoneNumber(phoneNumber).searchLead().selectResult().deleteButton().clickFindLeads().
		capturedLeadId(searchResult).searchLead();
	}
	
}
