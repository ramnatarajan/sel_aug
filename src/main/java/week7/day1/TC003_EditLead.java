package week7.day1;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC003_EditLead extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName = "TC002_CreateLead";
		testCaseDesc = "Create a Lead in Leaftaps";
		category = "Regression";
		author = "Ram Natarajan";
		testData = "EditLead";
	}
	
	@Test(dataProvider = "test data")
	public void editLead(String username, String password, String firstName, String compName) throws InterruptedException {
		
		new LoginPage().typeUserName(username).typePassword(password).eleLogin().clickcrmsfa().
		clickLeadsButton().clickFindLeads().searchLeads(firstName).searchLead().selectResult().
		editButton().updateCompanyName(compName).updateLead().verifyCompaName(compName).closeBrowser();
		
	}
}
