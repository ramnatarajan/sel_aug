package week7.day1;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName = "TC002_CreateLead";
		testCaseDesc = "Create a Lead in Leaftaps";
		category = "Regression";
		author = "Ram Natarajan";
		testData = "CreateLead";
	}
	
	@Test(dataProvider = "test data")
	public void createLead(String username, String password, String companyName, String firstName,
			String lastName) {
		new LoginPage().typeUserName(username).typePassword(password).eleLogin().clickcrmsfa().clickCreateLead()
		.typeCompName(companyName).typeFirstName(firstName).typeLastName(lastName).clickCreateLeadButton()
		.verifyCompaName(companyName);
	}
	
}
