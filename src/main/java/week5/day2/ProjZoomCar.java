package week5.day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class ProjZoomCar extends SeMethods{

	@Test
	public void zoomcar() throws InterruptedException{
		
		startApp("chrome", "https://www.zoomcar.com/chennai");
		WebElement startJourney = locateElement("xpath", "//a[@title='Start your wonderful journey']");
		click(startJourney);
		
		WebElement popularLoc = locateElement("xpath", "//div[@class='component-popular-locations']/div[5]");
		click(popularLoc);
		WebElement nextButton = locateElement("class", "proceed");
		click(nextButton);
		
		Thread.sleep(500);
		WebElement daySelect = locateElement("xpath", "(//div[@class='day'])");
		click(daySelect);
		WebElement slider = locateElement("xpath", "//div[@class='vue-slider-always vue-slider-dot']");
		clickAndHold(slider);
		dragAndDrop(slider, 501.628, 0);
		
		
	}
	
	
}
