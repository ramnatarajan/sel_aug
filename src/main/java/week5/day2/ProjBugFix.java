package week5.day2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class ProjBugFix {

	@Test
	public void mynthra() throws InterruptedException {

		// launch the browser
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.myntra.com/");

		// Mouse Over on Men
		Actions builder = new Actions(driver);
		builder.moveToElement(driver.findElementByLinkText("Men")).perform();

		// Click on Jackets
		driver.findElementByXPath("//a[@href='/men-jackets']").click();


		// Find the count of Jackets
		String leftCount = 
				driver.findElementByXPath("//span[@class='categories-num']")
				.getText().replaceAll("\\D", "");
		System.out.println(leftCount);


		// Click on Jackets and confirm the count is same
		driver.findElementByXPath("//label[text()='Jackets']").click();

		// Wait for some time
		Thread.sleep(5000);

		// Check the count
		String rightCount = 
				driver.findElementByXPath("//span[@class='horizontal-filters-sub']")
				.getText().replaceAll("\\D", "");
		System.out.println(rightCount);

		// If both count matches, say success
		if(leftCount.equals(rightCount)) {
			System.out.println("The count matches on either case");
		}else {
			System.err.println("The count does not match");
		}

		// Click on Offers
		try {
			driver.findElementByXPath("//h3[text()='Offers']").click();
		} catch (Exception e) {
			System.out.println("Offers button not found");
		}
		

		// Find the costliest Jacket
		List<WebElement> productsPrice = driver.findElementsByXPath("//span[@class='product-discountedPrice']");
		List<String> onlyPrice = new ArrayList<String>();

		for (WebElement productPrice : productsPrice) {
			onlyPrice.add(productPrice.getText().replaceAll("\\D", ""));
		}

		// Sort them 
		String max = Collections.max(onlyPrice);

		// Find the top one
		System.out.println(max);
		
		

		// Print Only Allen Solly Brand Minimum Price
		driver.findElementByXPath("//div[@class='brand-more']").click();
		//System.out.println("Line 81 executed");
		driver.findElementByXPath("//input[@value='Allen Solly']/following-sibling::div").click();
		//System.out.println("Line 83 executed");
		driver.findElementByXPath("//span[@class='myntraweb-sprite FilterDirectory-close sprites-remove']").click();
		//System.out.println("Line 85 executed");
		Thread.sleep(3000);

		// Find the costliest Jacket
		List<WebElement> allenSollyPrices = driver.findElementsByXPath("//span[@class='product-discountedPrice']");
		//System.out.println("Line 90 executed");
		//System.out.println(allenSollyPrices);
		onlyPrice = new ArrayList<String>();
		//System.out.println("Line 93 executed");
		
		for (WebElement productPrice : allenSollyPrices) {
			//Thread.sleep(800);
			onlyPrice.add(productPrice.getText().replaceAll("\\D", ""));
		}
		System.out.println("For loop executed");
		
		// Get the minimum Price 
		String min = Collections.min(onlyPrice);

		// Find the lowest priced Allen Solly
		System.out.println(min);

		driver.close();
	}

}

