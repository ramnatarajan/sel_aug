package week5.day1;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class CreateHtmlReport {

	public static void main(String[] args) throws IOException {
		//Creating html report file. Manually create the reports folder.
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/report1.html");
		
		//Append the test result in report
		html.setAppendExisting(false);
		//html.setAppendExisting(true);
		
		//Creating a constructor to generate the reports
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(html);
		
		//Test case level results
		ExtentTest test1 = extent.createTest("TC001_Login", "Login to Leaftaps with valid credentials");
		test1.assignCategory("Smoke test");
		test1.assignAuthor("Ram");
		
		//Test step level results
		test1.pass("Browser launched successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/firstwindow.png").build());
		test1.pass("Username entered successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/firstwindow.png").build());
		test1.pass("Password entered successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/firstwindow.png").build());
		
		//Mandatory.. This flushes the memory
		extent.flush();
	}

}
