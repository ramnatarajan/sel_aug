package week4.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateLead {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("HCL");
		driver.findElementById("createLeadForm_firstName").sendKeys("Ram");
		driver.findElementById("createLeadForm_lastName").sendKeys("Natarajan");
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Ram");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("Natarajan");
		driver.findElementByName("personalTitle").sendKeys("Mr.");
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Tech");
		WebElement source = driver.findElementById("createLeadForm_dataSourceId");
		Select value = new Select(source);
		value.selectByVisibleText("LEAD_EMPLOYEE");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("1000000");
		WebElement industry = driver.findElementById("createLeadForm_industryEnumId");
		Select value2 = new Select(industry);
		value2.selectByVisibleText("IND_SOFTWARE");
		WebElement owner = driver.findElementById("createLeadForm_ownershipEnumId");
		Select value3 = new Select(owner);
		value3.selectByVisibleText("OWN_PARTNERSHIP");
		driver.findElementById("createLeadForm_sicCode").sendKeys("78620");
		driver.findElementById("createLeadForm_description").sendKeys("This is a test");
		driver.findElementById("createLeadForm_importantNote").sendKeys("Nothing here");
		WebElement market = driver.findElementById("createLeadForm_marketingCampaignId");
		Select value4 = new Select(market);
		value4.selectByVisibleText("CATRQ_AUTOMOBILE");
		driver.findElementById("createLeadForm_departmentName").sendKeys("IT Department");
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("400");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("Nothing here");
		WebElement currency = driver.findElementById("createLeadForm_currencyUomId");
		Select value5 = new Select(currency);
		value5.selectByVisibleText("INR - Indian Rupee");
		
	}

}
