package week4.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SelectFrame {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://jqueryui.com");
		driver.findElementByXPath("//a[text()='Selectable']").click();
		driver.switchTo().frame(0);
		WebElement text = driver.findElementByXPath("//li[text()='Item 2']");
		text.click();
		
	}

}
