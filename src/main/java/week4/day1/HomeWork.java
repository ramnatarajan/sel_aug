package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class HomeWork {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[1]").click();
		Set<String> win = driver.getWindowHandles();
		List<String> windowlist = new ArrayList<String>();
		windowlist.addAll(win);
		driver.switchTo().window(windowlist.get(1));
		driver.findElementByName("id").sendKeys("10202");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		driver.findElementByLinkText("10202").click();
		driver.switchTo().window(windowlist.get(0));
		driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[2]").click();
		win = driver.getWindowHandles();
		windowlist = new ArrayList<String>();
		windowlist.addAll(win);
		driver.switchTo().window(windowlist.get(1));
		driver.findElementByName("id").sendKeys("10203");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		driver.findElementByLinkText("10203").click();
		driver.switchTo().window(windowlist.get(0));
		driver.findElementByLinkText("Merge").click();
		Alert alert = driver.switchTo().alert();
		String get = alert.getText();
		System.out.println(get);
		driver.switchTo().alert().accept();
		Thread.sleep(3000);
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByName("id").sendKeys("10203");
		driver.findElementByXPath("//button[text()='Find Leads']");
		Thread.sleep(5000);
		driver.close();
		
	}

}
