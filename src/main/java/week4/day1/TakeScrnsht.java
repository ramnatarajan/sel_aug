package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;

public class TakeScrnsht {

	public static void main(String[] args) throws WebDriverException, IOException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/eticketing/loginHome.jsf");
		driver.findElementByXPath("//span[text()='AGENT LOGIN']").click();
		driver.findElementByLinkText("Contact Us").click();
		Set<String> win = driver.getWindowHandles();
		List<String> windowlist = new ArrayList<String>();
		windowlist.addAll(win);
		driver.switchTo().window(windowlist.get(1));
		System.out.println(driver.getTitle());
		System.out.println(driver.getCurrentUrl());
		
		driver.switchTo().window(windowlist.get(0));
		FileUtils.copyFile(driver.getScreenshotAs(OutputType.FILE),new File("./snaps/firstwindow.png"));
		
		driver.close();
		
	}

}
