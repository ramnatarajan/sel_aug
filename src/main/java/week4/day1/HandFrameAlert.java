package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class HandFrameAlert {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.switchTo().frame(0);
		driver.findElementByXPath("//button[text()='Try it']").click();
		Alert alert = driver.switchTo().alert();
		String get = alert.getText();
		System.out.println(get);
		driver.switchTo().alert().sendKeys("Ram Natarajan");
		driver.switchTo().alert().accept();
		String b = driver.findElementById("demo").getText();
		System.out.println(b);
		
		driver.findElementByXPath("//button[text()='Try it']").click();
		Alert alert2 = driver.switchTo().alert();
		String get2 = alert.getText();
		System.out.println(get2);
		driver.switchTo().alert().dismiss();
		String c = driver.findElementById("demo").getText();
		System.out.println(c);
		driver.close();
	}

}
