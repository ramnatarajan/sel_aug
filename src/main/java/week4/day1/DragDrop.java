package week4.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class DragDrop {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://jqueryui.com/draggable/");
		driver.switchTo().frame(0);
		Actions action = new Actions(driver);
		WebElement drag = driver.findElementById("draggable");
		action.dragAndDropBy(drag, 130, 160).perform();

		driver.close();
	}

}
