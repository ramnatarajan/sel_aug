package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import wdMethods.SeMethods;

public class LoginPage extends SeMethods{

	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id = "username")
	WebElement eleUserName;
	@And("Enter the username as (.*)")
	public LoginPage typeUserName(String username) {
		type(eleUserName, username);
		return this;
	}
	
	@FindBy(id = "password")
	WebElement elePassword;
	@And("Enter the password as (.*)")
	public LoginPage typePassword(String password) {
		type(elePassword, password);
		return this;
	}
	
	@FindBy(how = How.CLASS_NAME, using ="decorativeSubmit")
	WebElement clickLogin;
	@When("Click on login button")
	public HomePage eleLogin() {
		click(clickLogin);
		return new HomePage();
	}
		
}
