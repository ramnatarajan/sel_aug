package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewLead extends ProjectMethods {

	public ViewLead() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(linkText = "Edit")
	WebElement clickEdit;
	public EditLeadPage editButton() {
		click(clickEdit);
		return new EditLeadPage();
	}
	
	@FindBy(linkText = "Delete")
	WebElement clickDelete;
	public MyLeads deleteButton() {
		click(clickDelete);
		return new MyLeads();
	}
	
	
	@FindBy(linkText = "Duplicate Lead")
	WebElement dupeLeadButton;
	public DuplicateLeadPage duplicateLeadButton() {
		click(dupeLeadButton);
		return new DuplicateLeadPage();
	}
	
	
}
