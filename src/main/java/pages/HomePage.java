package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import wdMethods.SeMethods;

public class HomePage extends SeMethods{

	public HomePage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//h2[text()='Demo Sales Manager']")
	WebElement vUserName;
	@Then("Verify login is successful")
	public HomePage afterLoginPage(String data) {
		verifyPartialText(vUserName, data);
		return this;
	}
	
	@FindBy(linkText = "CRM/SFA")
	WebElement clickCrmSfa;
	@And("Click CRMSFA link")
	public MyHomePage clickcrmsfa() {
		click(clickCrmSfa);
		return new MyHomePage();
	}

}
