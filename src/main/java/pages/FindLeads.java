package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeads extends ProjectMethods {

	public FindLeads() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(name = "firstName")
	WebElement findFirstName;
	public FindLeads searchLeads(String data) {
		type(findFirstName, data);
		return this;
	}
	
	@FindBy(xpath = "//button[text()='Find Leads']")
	WebElement clickFindLeadsButton;
	public FindLeads searchLead() throws InterruptedException {
		click(clickFindLeadsButton);
		Thread.sleep(500);
		return this;
	}
	
	@FindBy(xpath = "(//a[@class='linktext'])[4]")
	WebElement clickResult;
	public ViewLead selectResult() {
		String firstSearchResult = clickResult.getText();
		click(clickResult);
		return new ViewLead();
	}
	
	@FindBy(xpath = "//span[text()='Phone']")
	WebElement phoneTab;
	public FindLeads clickPhoneTab() {
		click(phoneTab);
		return this;
	}
	
	@FindBy(name = "phoneCountryCode")
	WebElement countryCode;
	public FindLeads enterCountryCode(String data){
		type(countryCode, data);
		return this;
	}
	
	@FindBy(name = "phoneNumber")
	WebElement phoneNumber;
	public FindLeads enterPhoneNumber(String data) {
		type(phoneNumber, data);
		return this;
	}
	
	
	@FindBy(xpath = "(//a[@class='linktext'])[4]")
	WebElement firstResult;
	public ViewLead clickFirstResult() {
		click(firstResult);
		return new ViewLead();
	}
	
	
	@FindBy(name = "id")
	WebElement capLeadId;
	public FindLeads capturedLeadId(String firstSearchResult) {
		type(capLeadId, firstSearchResult);
		return this;
	}
	
	
	@FindBy(xpath = "(//span[@class='x-tab-strip-text '])[3]")
	WebElement clickEmail;
	public FindLeads emailTab() {
		click(clickEmail);
		return this;
	}
	
	@FindBy(name = "emailAddress")
	WebElement enterEmail;
	public FindLeads typeEmail(String data) {
		type(enterEmail, data);
		return this;
	}
	
}
