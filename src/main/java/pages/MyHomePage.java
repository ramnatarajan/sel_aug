package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.SeMethods;

public class MyHomePage extends SeMethods{

	public MyHomePage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(linkText = "Create Lead")
	WebElement createLead;
	@And("Click Create Lead link")
	public CreateLeadPage clickCreateLead() {
		click(createLead);
		return new CreateLeadPage();
	}
	
	@FindBy(linkText = "Leads")
	WebElement leadsButton;
	public MyLeads clickLeadsButton() {
		click(leadsButton);
		return new MyLeads();
	}
	
}
