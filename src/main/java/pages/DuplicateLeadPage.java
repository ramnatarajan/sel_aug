package pages;

import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class DuplicateLeadPage extends ProjectMethods {

	public DuplicateLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	public DuplicateLeadPage verifyPageTitle(String pageTitle) {
				verifyTitle(pageTitle);
				return this;
	}
}