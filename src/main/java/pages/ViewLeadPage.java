package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Then;
import wdMethods.SeMethods;

public class ViewLeadPage extends SeMethods{

	public ViewLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy (id = "viewLead_companyName_sp")
	WebElement compName;
	@Then("Verify the company name as (.*)")
	public ViewLeadPage verifyCompaName(String data) {
		verifyExactText(compName, data);
		return this;
	}
	
}
