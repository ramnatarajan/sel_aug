package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import wdMethods.SeMethods;

public class CreateLeadPage extends SeMethods{

	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy (id = "createLeadForm_companyName")
	WebElement companyName;
	@And("Enter Company name as (.*)")
	public CreateLeadPage typeCompName(String data) {
		type(companyName, data);
		return this;
	}
	
	@FindBy (id = "createLeadForm_firstName")
	WebElement firstName;
	@And("Enter First name as (.*)")
	public CreateLeadPage typeFirstName(String data){
		type(firstName, data);
		return this;
	}
	
	
	@FindBy (id = "createLeadForm_lastName")
	WebElement lastName;
	@And("Enter Last name as (.*)")
	public CreateLeadPage typeLastName(String data) {
		type(lastName, data);
		return this;
	}
	
	@FindBy (name = "submitButton")
	WebElement createLeadButton;
	@When("Click on Create Lead button")
	public ViewLeadPage clickCreateLeadButton() {
		click(createLeadButton);
		return new ViewLeadPage();
	}
}
