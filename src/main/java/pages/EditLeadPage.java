package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class EditLeadPage extends ProjectMethods{

	public EditLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id = "updateLeadForm_companyName")
	WebElement updateCompName;
	public EditLeadPage updateCompanyName(String data) {
		type(updateCompName, data);
		return this;
	}
	
	@FindBy(name = "submitButton")
	WebElement clickUpdateButton;
	public ViewLeadPage updateLead() {
		click(clickUpdateButton);
		return new ViewLeadPage();
	}
	
}
