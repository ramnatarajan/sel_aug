package week6.classwork;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;




public class TC006_DeleteLead extends ProjectMethods{
	
	@BeforeTest (groups = {"sanity"})
	public void setData() {
		testCaseName = "TC002_CreateLead";
		testCaseDesc = "Create a Lead in Leaftaps";
		category = "Sanity";
		author = "Ram Natarajan";
	}
	@Test (dataProvider = "delete lead test data")
	public void deleteLead(String phoneNum) throws InterruptedException {
				
		WebElement leads = locateElement("linkText", "Leads");
		click(leads);
		WebElement findleads = locateElement("linkText", "Find Leads");
		click(findleads);
		WebElement phone = locateElement("xpath", "(//span[@class='x-tab-strip-text '])[2]");
		click(phone);
		/*WebElement countryCode = locateElement("name", "phoneCountryCode");
		type(countryCode, "91");
		WebElement areaCode = locateElement("name", "phoneAreaCode");
		type(areaCode, "44");*/
		WebElement phoneNumber = locateElement("name", "phoneNumber");
		type(phoneNumber, phoneNum);		
		WebElement findLead = locateElement("xpath", "//button[text()='Find Leads']");
		click(findLead);
		Thread.sleep(500);
		WebElement result = locateElement("xpath", "(//a[@class='linktext'])[4]");
		System.err.println(getText(result));
		String s = getText(result);
		click(result);
		Thread.sleep(500);
		WebElement deleteButton = locateElement("class", "subMenuButtonDangerous");
		click(deleteButton);
		WebElement findleads2 = locateElement("linkText", "Find Leads");
		click(findleads2);
		WebElement leadId = locateElement("name", "id");
		type(leadId, s);
		click(findLead);
		Thread.sleep(500);

		closeBrowser();
		
	}
	
	@DataProvider(name="delete lead test data")
	public Object[][] testData(){
		Object[][] data = new Object[1][1];
		
		data[0][0] = "9933223340";
		
		return data;
		
	}

	
}
