package week6.classwork;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;




public class TC005_DuplicateLead extends ProjectMethods{
	
	@BeforeTest (groups = {"regression"})
	public void setData() {
		testCaseName = "TC002_CreateLead";
		testCaseDesc = "Create a Lead in Leaftaps";
		category = "Regression";
		author = "Ram Natarajan";
	}
	@Test (dataProvider = "duplicate lead test data")
	public void duplicateLead(String emailId) throws InterruptedException {
				
		WebElement leads = locateElement("linkText", "Leads");
		click(leads);
		WebElement findleads = locateElement("linkText", "Find Leads");
		click(findleads);
		WebElement email = locateElement("xpath", "(//span[@class='x-tab-strip-text '])[3]");
		click(email);
		WebElement enterEmail = locateElement("name", "emailAddress");
		type(enterEmail, emailId);
		WebElement findleads2 = locateElement("xpath", "//button[text()='Find Leads']");
		click(findleads2);
		Thread.sleep(500);
		WebElement firstName = locateElement("xpath", "(//a[@class='linktext'])[6]");
		System.out.println(getText(firstName));
		WebElement result = locateElement("xpath", "(//a[@class='linktext'])[4]");
		click(result);
		WebElement duplicateLead = locateElement("linkText", "Duplicate Lead");
		click(duplicateLead);
		System.out.println(verifyTitle(getTitle()));
		WebElement createLead = locateElement("class", "smallSubmit");
		click(createLead);
		WebElement duplicateName = locateElement("id", "viewLead_firstName_sp");
		if(duplicateName.equals(firstName)) {
			System.out.println("Duplicated lead name is same as captured name");
		}else {
			System.out.println("Duplicated lead name doesn't match captured name");
		}
		
		closeBrowser();
		
	}
	
	@DataProvider (name="duplicate lead test data")
	
	public Object[][] testData(){
		
		Object[][] data = new Object[1][1];
		
		data [0][0] = "ramnatrajan@gmail.com";
		
		return data;
		
	}

	
}
