package week6.day1;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;




public class TC005_DuplicateLead extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC002_CreateLead";
		testCaseDesc = "Create a Lead in Leaftaps";
		category = "Regression";
		author = "Ram Natarajan";
	}
	@Test (enabled = false)
	public void duplicateLead() {
				
		WebElement leads = locateElement("linkText", "Leads");
		click(leads);
		WebElement findleads = locateElement("linkText", "Find Leads");
		click(findleads);
		WebElement firstname = locateElement("xpath", "//input[@id='ext-gen248']");
		type(firstname, "Ram");
		WebElement findleads2 = locateElement("xpath", "//button[text()='Find Leads']");
		click(findleads2);
		WebElement result = locateElement("xpath", "//a[@id='ext-gen940']");
		click(result);
		
		verifyTitle(getTitle());
	
		WebElement editLead = locateElement("linkText", "Edit");
		click(editLead);
		WebElement clear = locateElement("id", "updateLeadForm_companyName");
		clear.clear();
		
		WebElement compName = locateElement("id", "updateLeadForm_companyName");
		type(compName, "HCL");
		WebElement update = locateElement("name", "submitButton");
		click(update);
		
		WebElement verify = locateElement("id", "viewLead_companyName_sp");
		System.out.println(verify.getText());
		
		closeBrowser();
		
	}
	

	
}
