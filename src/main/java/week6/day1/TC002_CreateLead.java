package week6.day1;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;


public class TC002_CreateLead extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName = "TC002_CreateLead";
		testCaseDesc = "Create a Lead in Leaftaps";
		category = "Regression";
		author = "Ram Natarajan";
	}
	
	@Test (invocationCount = 2)
	public void createLead() {
		
		WebElement create = locateElement("linkText", "Create Lead");
		click(create);
		WebElement companyname = locateElement("id", "createLeadForm_companyName");
		type(companyname, "HCL");
		WebElement firstname = locateElement("id", "createLeadForm_firstName");
		type(firstname, "Ram");
		WebElement lastname = locateElement("id", "createLeadForm_lastName");
		type(lastname, "Natarajan");
		WebElement source = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(source, "Employee");
		WebElement marketing = locateElement("id", "createLeadForm_marketingCampaignId");
		selectDropDownUsingText(marketing, "Automobile");
		WebElement firstnamelocal = locateElement("id", "createLeadForm_firstNameLocal");
		type(firstnamelocal, "Ram");
		WebElement lastnamelocal = locateElement("id", "createLeadForm_lastNameLocal");
		type(lastnamelocal, "Natarajan");
		WebElement salute = locateElement("id", "createLeadForm_personalTitle");
		type(salute, "Mr.");
		WebElement title = locateElement("id", "createLeadForm_generalProfTitle");
		type(title, "Nothing here");
		WebElement dept = locateElement("id", "createLeadForm_departmentName");
		type(dept, "Software");
		WebElement rev = locateElement("id", "createLeadForm_annualRevenue");
		type(rev, "100000");
		WebElement currency = locateElement("id", "createLeadForm_currencyUomId");
		selectDropDownUsingText(currency, "INR - Indian Rupee");
		WebElement indus = locateElement("id", "createLeadForm_industryEnumId");
		selectDropDownUsingIndex(indus, 4);
		WebElement owner = locateElement("id", "createLeadForm_ownershipEnumId");
		selectDropDownUsingText(owner, "Corporation");
		WebElement siid = locateElement("id", "createLeadForm_sicCode");
		type(siid, "Nothing");
		WebElement desc = locateElement("id", "createLeadForm_description");
		type(desc, "This is description");
		WebElement imp = locateElement("id", "createLeadForm_importantNote");
		type(imp, "Important note here");
		WebElement ticker = locateElement("id", "createLeadForm_tickerSymbol");
		type(ticker, "Tick tick tick");
		WebElement countrycode = locateElement("id", "createLeadForm_primaryPhoneCountryCode");
		type(countrycode, "91");
		WebElement area = locateElement("id", "createLeadForm_primaryPhoneAreaCode");
		type(area, "044");
		WebElement phone = locateElement("id", "createLeadForm_primaryPhoneNumber");
		type(phone, "044");
		WebElement extn = locateElement("id", "createLeadForm_primaryPhoneExtension");
		type(extn, "71654");
		WebElement askfor = locateElement("id", "createLeadForm_primaryPhoneAskForName");
		type(askfor, "Nobody");
		WebElement url = locateElement("id", "createLeadForm_primaryWebUrl");
		type(url, "No website for me");
		WebElement email = locateElement("id", "createLeadForm_primaryEmail");
		type(email, "ramnatrajan@gmail.com");
		WebElement genName = locateElement("id", "createLeadForm_generalToName");
		type(genName, "Chris");
		WebElement attnName = locateElement("id", "createLeadForm_generalAttnName");
		type(attnName, "Oliver");
		WebElement addr1 = locateElement("id", "createLeadForm_generalAddress1");
		type(addr1, "Blah blah blah");
		WebElement addr2 = locateElement("id", "createLeadForm_generalAddress2");
		type(addr2, "Blah blah blah");
		WebElement city = locateElement("id", "createLeadForm_generalCity");
		type(city, "Chennai");
		WebElement postal = locateElement("id", "createLeadForm_generalPostalCode");
		type(postal, "600077");
		WebElement country = locateElement("id", "createLeadForm_generalCountryGeoId");
		selectDropDownUsingText(country, "India");
		//Thread.sleep(3000);
		WebElement state = locateElement("id", "createLeadForm_generalStateProvinceGeoId");
		selectDropDownUsingText(state, "TAMILNADU");
		WebElement postalextn = locateElement("id", "createLeadForm_generalPostalCodeExt");
		type(postalextn, "982");
		WebElement createLead = locateElement("name", "submitButton");
		click(createLead);
		
		driver.close();		
		
	}
	
}
