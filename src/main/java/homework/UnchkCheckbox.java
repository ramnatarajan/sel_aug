package homework;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class UnchkCheckbox {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://testleaf.herokuapp.com/pages/checkbox.html");
		driver.findElementByXPath("//body/div/div/div[3]/section/div[3]/input[2]").click();
		
		Thread.sleep(5000);
		driver.close();

	}

}
