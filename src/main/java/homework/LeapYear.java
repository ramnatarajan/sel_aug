package homework;

import java.util.Scanner;

public class LeapYear {

	public static void main(String[] args) {
		int year;
		boolean b = false;
		System.out.println("Enter the year: ");
		Scanner scan = new Scanner(System.in);
		year = scan.nextInt();
		if (year % 4==0) {
			if(year % 100 == 0) {
				if(year % 400 ==0) {
					b = true;
				}
				else {
					b = false;
				}
			}
			else {
				b = true;
			}
		}
		else {
			b = false;
		}
		if (b==true) {
			System.out.println(year+" is a leap year");
		}
		else {
			System.out.println(year+" is not a leap year");
		}
	}

}
