Feature: Create lead in leaftaps

#Background:
#Given Open the Chrome browser
#And Maximize the browser
#And Set timeouts
#And Enter the URL

Scenario Outline: Positive flow to create a lead

And Enter the username as <username>
And Enter the password as <password>
When Click on login button
#Then Verify login is successful <vusername>
And Click CRMSFA link
And Click Create Lead link
And Enter Company name as <companyname>
And Enter First name as <firstname>
And Enter Last name as <lastname>
When Click on Create Lead button
Then Verify the company name as <compname>
#And Close the Browser

Examples:
|username|password|companyname|firstname|lastname|compname|
|DemoSalesManager|crmsfa|HCL|Ram|Natarajan|HCL|

