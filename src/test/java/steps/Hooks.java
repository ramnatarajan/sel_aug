package steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import wdMethods.SeMethods;

public class Hooks extends SeMethods{

	@Before
	public void beforeScenario(Scenario scen) {
		System.out.println(scen.getName());
		beginResult();
		testCaseName = scen.getName();
		testCaseDesc = scen.getId();
		category = "Regression";
		author = "Ram Natarajan";
		startTestCase();
		startApp("chrome", "http://leaftaps.com/opentaps/");
	}
	
	@After
	public void afterScenario(Scenario scen) {
		System.out.println(scen.getId());
		System.out.println(scen.isFailed());
		closeBrowser();
		endResult();
	}
}
