package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLead {

	public ChromeDriver driver;
	
	/*@Given("Open the Chrome browser")
	public void launchBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
	}
	
	@And("Maximize the browser")
	public void maxBrowser() {
		driver.manage().window().maximize();
	}
	
	@And("Set timeouts")
	public void waitTimeout() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	@And("Enter the URL")
	public void enterURL() {
		driver.get("http://leaftaps.com/opentaps/");
	}
	
	@And("Enter the username as (.*)")
	public void enterUserName(String userName) {
		driver.findElementById("username").sendKeys(userName);
	}

	@And("Enter the password as (.*)")
	public void enterpassWord(String passWord) {
		driver.findElementById("password").sendKeys(passWord);
	}
	
	@When("Click on login button")
	public void clickLoginButton() {
		driver.findElementByClassName("decorativeSubmit").click();
	}

	@Then("Verify login is successful")
	public void verifyLogin() {
		System.out.println(driver.getTitle());
	}
	
	@And("Click CRMSFA link")
	public void clickCrmSfa() {
		driver.findElementByLinkText("CRM/SFA").click();
	}
	
	@And("Click Create Lead link")
	public void createLeadLink() {
		driver.findElementByLinkText("Create Lead").click();
	}
	
	@And("Enter Company name as (.*)")
	public void enterCompName(String companyName) {
		driver.findElementById("createLeadForm_companyName").sendKeys(companyName);
	}
	
	@And("Enter First name as (.*)")
	public void enterFirstName(String firstName) {
		driver.findElementById("createLeadForm_firstName").sendKeys(firstName);
	}
	
	@And("Enter Last name as (.*)")
	public void enterLastName(String lastName) {
		driver.findElementById("createLeadForm_lastName").sendKeys(lastName);
	}
	
	@When("Click on Create Lead button")
	public void clickCreateLead() {
		driver.findElementByName("submitButton").click();
	}
	
	@Then("Verify the page title")
	public void verifypageTitle() {
		System.out.println(driver.getTitle());
	}
	
	@And("Close the Browser")
	public void closeBrowser() {
		driver.close();*/
	}

